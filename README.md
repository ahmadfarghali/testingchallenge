# Robusta's Testing Task 

* Creating an Analysis Document and performing Manual Testing. It can viewed from [Here](https://drive.google.com/file/d/18eOXdwOVGPyakD5qmDud5FYNLvoNiAD1/view?usp=sharing)
* Performing Automation Testing on the required Modules 

## Technologies and Tools
* [Cypress 9.5.0](https://www.cypress.io/) : Cypress is the next generation front end testing tool built for the modern web which focuses on the end to end testing.
* [Mochawesome Reporter](https://www.npmjs.com/package/mochawesome): Mochawesome is a custom reporter to be used with  mocha, the javascript testing framework. It runs on Node.js  and works in conjunction with mochawesome-report-generator to generate a standalone HTML/CSS report for visualizing the reports in an elegant way.
* [ESLINT](https://eslint.org/): Basically a code linting tool that is used for identifying and reporting on patterns found in JavaScript code, with the goal of making code more consistent and easily readable.
* [VSCode](https://code.visualstudio.com/): Visual Studio Code  is an open source, powerful, lightweight code editor that combines the simplicity of a source code editor with powerful developer tooling, like IntelliSense code completion and debugging.

## Installation

* Clone the project using HTTPS
```bash
git clone -b <branchname> https://gitlab.com/ahmadfarghali/testingchallenge.git
```
* Run the command below to install Cypress inside the cloned directory.
``` bash
npm install cypress --save-dev
```

## Usage

 To fire up the cypress runner and execute tests separately, use the following command
```bash
npx cypress open
```
To get detailed reports of the tests as a whole or individually use the following command
```bash
npx cypress run
```

- The reports will be saved in the folder called /reports/



## Important Notes
* I have intentionally not implemented the **Modular Design** because when creating these kind of tests it is preferred not to use the Page Object model for many reasons such as:-
1. Page objects are hard to maintain and often consume time from the actual development.
2. Using App actions is much faster than using Page objects in terms of performance and speed.\
--> You can read more about about  why it is encouraged to use App Actions more than Page Objects [link](https://www.cypress.io/blog/2019/01/03/stop-using-page-objects-and-start-using-app-actions/)
---
* It is not recommended to automate the **CAPTCHA**, in fact, Best practices is to not automate such as well as it should be disabled in the test environment by simple configuration in the software during testing or by setting a URL parameter.Moreover, When automating the registration flow that has the **OTP verification** included in the process , it is highly recommended to disable such on the test environment while tests are running. With that being said , I did not attempt to automate either.
\
-->You  can read more about it [here](https://www.pcloudy.com/blogs/testing-scenarios-you-should-avoid-while-automating-with-selenium/)

---

##
